# AXA / BI
## Frontend Technical Test

## Instructions

As a insurance company we've been asked to develop an application that
manages some information about our insurance policies and company clients.
To do that, we need to create a cool website that shows the list of
clients, and allows us to view the policies linked to a selected client.
Only users that are included in the list, can access the application.
We have the following constraints:
 * Website should be developed using ~~Angular/Knockout~~ React.
 * CSS is required - will use Bootstrap v4 for CSS
 * ~~Typescript is required~~ ES6 
 * The JSON list of policies can be collected from:
http://www.mocky.io/v2/580891a4100000e8242b75c5
 * The JSON list of users can be collected from:
http://www.mocky.io/v2/5808862710000087232b75ac
 * The list cannot be accessed without previous authentication (check
client's email)

As our stakeholders are very fussy, here some tips:
 * Usage of last technologies
 * Solution properly structured
 * Usage of patterns 

## To do
- [x] Create repo
- [x] Init Readme
- [x] ~~Start~~ Finish app

## Installation and Usage

After cloning the repository, run `npm install` to install dependencies; `npm start` to open a webpack-dev-server on port 3000


## Requirements

This project was bootstrapped with the new `create-react-app 2.0` so I can only recommend the latest version of `node` to date (`v10.11.0`).
It will fail to build with versions older than v9 (most probably)