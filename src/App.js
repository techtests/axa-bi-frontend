import React, { Component } from 'react'

import axios from 'axios'

import Login from './components/login'
import Policies from './components/policies'

import URL from './constants/urls'

import './App.css'

class App extends Component {
  constructor () {
    super()
    this.state = {
      user: null,
      policies: null,
      notFound: null
    }
    // Bind context
    this.onLogin = this.onLogin.bind(this)
  }
  
  // Handler
  onLogin (e) {
    const email = e.target.loginFormEmail.value
    e.preventDefault()

    axios.get(URL.clients).then(
      clients => {
        let user = clients.data.clients.filter(client => client.email === email)[0]
        if (user){
          axios.get(URL.policies).then(
            policies => {
              this.setState({ user, policies: policies.data.policies, notFound: null })
            },
            error => { console.log(error) }
          )
        } else {
          this.setState({ user: null, notFound: 'Invalid e-mail or not found.' })
        }
      },
      error => { console.log(error) }
    )
  }

  // Conditionally render container
  renderPage () {
    if (this.state.user) return (<Policies list={this.state.policies} user={this.state.user} />)
    return (<Login onSubmit={this.onLogin} notFound={this.state.notFound} />)
  }

  render () {
    return (
      <div className="App container">
        <div className="row">
          <div className="col-md-3 offset-md-3 mt-5">
            <h1>Policies test</h1>
          </div>
        </div>
        { this.renderPage() }
      </div>
    )
  }
}

export default App;
