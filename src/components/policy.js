import React from 'react'

const PolicyItem = ({data}) => (
  <li>
    <ul>
      <li>
        Date: { data.inceptionDate }
      </li>
      <li>
        Amount insured: { data.amountInsured }
      </li>
      <li>
        Installment Payment: { data.installmentPayment ? 'Yes' : 'No' }
      </li>
    </ul>
  </li>
)

export default PolicyItem
