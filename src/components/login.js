import React from 'react'

const notFound = message => {
  if (!message) return null
  return (
    <div className="alert alert-danger" role="alert">
      <strong>Error:</strong> { message }
    </div>
  )
}

const LoginForm = props => {
  return (
    <div className="row align-items-center mt-5">
      <div className="col-md-4 offset-md-4">
        <form onSubmit={props.onSubmit}>
          <div className="form-group">
            <label htmlFor="email">
              E-Mail
            </label>
            <input placeholder="user@service.com" className="form-control" name="loginFormEmail" />
            <small id="loginEmailHelp" className="form-text text-muted">You need to be a client to check your policies</small>
          </div>
          { notFound(props.notFound) }
          <button type="submit" className="btn btn-primary">Login</button>
        </form>
      </div>
    </div>
  )
}

export default LoginForm
