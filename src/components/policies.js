import React from 'react'
import Policy from './policy'

const message = amount => {
  if (amount) return (<p>These are your policies:</p>)
  return (<p>You have no policies</p>)
}

const list = (list, email) => {
  const policyItems = list.filter(policy => policy.email === email).map(policy => <Policy key={policy.id} data={policy} />)
  return (
    <div>
      { message(policyItems.length) }
      <ul>
        { policyItems }
      </ul>
    </div>
  )
}

const PoliciesList = props => {
  return (
    <div className="row align-items-center mt-2">
      <div className="col-md-6 offset-md-3">
        <h3>Hello {props.user.name}</h3>
        { list(props.list, props.user.email) }
      </div>
    </div>
  )
}

export default PoliciesList
